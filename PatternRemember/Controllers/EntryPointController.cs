﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PatternRemember.Models.DataModels;
using System.Text.RegularExpressions;
using System.Web.Routing;

namespace PatternRemember.Controllers
{
    public class EntryPointController : Controller
    {
        private PatternDBContext db = new PatternDBContext();

        // GET: EntryPoint
        public ActionResult Index()
        {
            return View(db.EntryPoint.ToList());
        }

        // GET: EntryPoint/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EntryPoint entryPoint = db.EntryPoint.Find(id);
            if (entryPoint == null)
            {
                return HttpNotFound();
            }
            return View(entryPoint);
        }

        // GET: EntryPoint/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: EntryPoint/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "EntryPointId,BeforeFormat,AfterFormat,ProccessedV1,ProccessedV2")] EntryPoint entryPoint)
        {
            if (ModelState.IsValid)
            {
                string ProcessedText = entryPoint.AfterFormat;

                //Case 1 (if format text is null - Process(Format) the FormattedText based on existing data in Memory table. No new learning data will be entered in Memory
                if (entryPoint.AfterFormat == null)
                {


                }
                //Case 2 (if format text is not null - Process(Format) the FormattedText based on existing + new data in Memory table. New learning data will be entered in Memory
                else
                {

                    string BeforeFormatString = entryPoint.BeforeFormat.Trim();
                    string AfterFormatString = entryPoint.AfterFormat.Trim();
                    int countOfBeforeFormatText = WordCounter(BeforeFormatString);
                    int countOfAfterFormatText = WordCounter(AfterFormatString);
                    
                    string newText = "";

                    //Subcase A - same words (maybe only updated words)
                    if (countOfBeforeFormatText == countOfAfterFormatText)
                    {
                        //learn updated words
                        LearnUpdatedWordsWhenCountIsSame(BeforeFormatString, AfterFormatString);
                        List<Memory> MemoryData = new List<Memory>();
                        MemoryData = db.Memory.ToList();
                        foreach (var dataItem in MemoryData)
                        {
                            
                            newText = ProcessedText.Replace(dataItem.Find, dataItem.Replace);
                        }
                        entryPoint.ProccessedV1 = newText;

                    }
                    //Subcase B - new words added
                    else if (countOfAfterFormatText >= countOfBeforeFormatText)
                    {
                       
                        LearnUpdatedWordsWhenAfterFormatCountIsHigher(BeforeFormatString, AfterFormatString);
                        List<Memory> MemoryData = new List<Memory>();
                        MemoryData = db.Memory.ToList();
                        foreach (var dataItem in MemoryData)
                        {
                            if (!ProcessedText.Contains(dataItem.Replace))
                            {
                                newText = ProcessedText.Replace(dataItem.Find, dataItem.Replace);
                            }
                            
                        }
                        entryPoint.ProccessedV1 = newText;

                    }

                    //Subcase C - words deleted


                }


                //Regular code
                db.EntryPoint.Add(entryPoint);
                db.SaveChanges();              
                return RedirectToAction("Details", new RouteValueDictionary(new { controller = "EntryPoint", action = "Details", Id = entryPoint.EntryPointId }));
            }

            return View(entryPoint);
        }

        // GET: EntryPoint/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EntryPoint entryPoint = db.EntryPoint.Find(id);
            if (entryPoint == null)
            {
                return HttpNotFound();
            }
            return View(entryPoint);
        }

        // POST: EntryPoint/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "EntryPointId,BeforeFormat,AfterFormat,ProccessedV1,ProccessedV2")] EntryPoint entryPoint)
        {
            if (ModelState.IsValid)
            {
                db.Entry(entryPoint).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(entryPoint);
        }

        // GET: EntryPoint/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EntryPoint entryPoint = db.EntryPoint.Find(id);
            if (entryPoint == null)
            {
                return HttpNotFound();
            }
            return View(entryPoint);
        }

        // POST: EntryPoint/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            EntryPoint entryPoint = db.EntryPoint.Find(id);
            db.EntryPoint.Remove(entryPoint);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


        public int WordCounter(string text)
        {
            int wordCount = 0, index = 0;
            while (index < text.Length)
            {
                // check if current char is part of a word
                while (index < text.Length && !char.IsWhiteSpace(text[index]))
                    index++;

                wordCount++;

                // skip whitespace until next word
                while (index < text.Length && char.IsWhiteSpace(text[index]))
                    index++;
            }
            return wordCount;
        }

        public void LearnUpdatedWordsWhenCountIsSame(string firstString, string secondString)
        {
            string[] firstStringWords = firstString.Split(' ');
            string[] secondStringWords = secondString.Split(' ');
            int cc = secondStringWords.Count();

            for (int i = 0; i < cc; i = i+1)
            {
                int a = i;
                //int ParentCounterLoop = 0;
                if(firstStringWords[i] != secondStringWords[i])
                {
                    //Add Entry in Data Memory table
                    Memory LearningData = new Memory();
                    LearningData.Find = firstStringWords[i];
                    LearningData.Replace = secondStringWords[i];
                    db.Memory.Add(LearningData);
                    db.SaveChanges();
                }

                //ParentCounterLoop = ParentCounterLoop++;

                //foreach (var wordChild in firstStringWords)
                //{
                //    int ChildCounterLoop = 1;

                //    if (wordChild == word)
                //    {
                //        break;
                //    }
                //    else
                //    {
                //        //Add Entry in Data Memory table
                //        Memory LearningData = new Memory();
                //        LearningData.Find = wordChild;
                //        LearningData.Replace = word;
                //        db.Memory.Add(LearningData);
                //        db.SaveChanges();
                //    }


                //    ChildCounterLoop = ChildCounterLoop++;
                //}
                

            }


        }


        public void LearnUpdatedWordsWhenAfterFormatCountIsHigher(string firstString, string secondString)
        {
            string[] firstStringWords = firstString.Split(' ');
            string[] secondStringWords = secondString.Split(' ');
            int cc = firstStringWords.Count();
            //foreach (string fsWord in firstStringWords)
            for (int i = 0; i < cc; i = i + 1)
            {
                if (firstStringWords[i] != secondStringWords[i])
                {
                    //Function that finds the nth position of word between passed index(Index of Before Format)
                    int getNthWord = getNthWordOfMatchedWord(secondString, firstStringWords[i]);
                    string Find = firstStringWords[i];
                    string Replace = "";
                    if (getNthWord >= 3)
                    {
                        Replace = secondStringWords[getNthWord - 3] + " " + secondStringWords[getNthWord - 2] + " " + secondStringWords[getNthWord - 1];

                    } else if (getNthWord == 2)
                    {
                        Replace = secondStringWords[getNthWord - 2] + " " + secondStringWords[getNthWord - 1];

                    } else if (getNthWord == 1)
                    {
                        Replace = secondStringWords[getNthWord - 1];

                    }


                    //Add Entry in Data Memory table
                    if (Replace != "")
                    {
                        Memory LearningData = new Memory();
                        LearningData.Find = Find;
                        LearningData.Replace = Replace;
                        db.Memory.Add(LearningData);
                        db.SaveChanges();
                    }
                }

            }

        }



        public int getNthWordOfMatchedWord(string statement, string find)
        {
            //statement = "I am going to School I am going to School";
            //find = "School";
            //int[] resultoutput;           
            // Call Regex.Matches method.
            MatchCollection matches = Regex.Matches(statement, find);
            int NthWord = 0;
            string output = "";

            // Loop over matches.
            foreach (Match m in matches)
            {
                
                // Loop over captures.
                foreach (Capture c in m.Captures)
                {
                    int j = 0;
                    // Display.
                    output += c.Index.ToString() + ",";

                    string passedString = statement.Substring(0, c.Index);//statement.PadLeft(c.Index);
                    int countSpaces = passedString.Count(char.IsWhiteSpace);
                    NthWord = countSpaces + 1;

                    //resultoutput[j] = c.Index;

                    j = j + 1;
                }
            }

            ViewBag.output = output;
            return NthWord;
        }













    }
}
