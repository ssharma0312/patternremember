﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PatternRemember.Models.DataModels
{
    public class Memory
    {
        [Key]
        public int MemoryId { get; set; }
        public string Find { get; set; }
        public string Replace { get; set; }
    }
}