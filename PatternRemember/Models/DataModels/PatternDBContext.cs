namespace PatternRemember.Models.DataModels
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class PatternDBContext : DbContext
    {
        public PatternDBContext()
            : base("name=PatternDBContext")
        {
        }

        //1
        public virtual DbSet<EntryPoint> EntryPoint { get; set; }

        //2
        public virtual DbSet<Memory> Memory { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
