﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PatternRemember.Models.DataModels
{
    public class EntryPoint
    {
        [Key]
        public int EntryPointId { get; set; }
        public string BeforeFormat { get; set; }
        public string AfterFormat { get; set; }
        public string ProccessedV1 { get; set; }
        public string ProccessedV2 { get; set; }
      
    }
}